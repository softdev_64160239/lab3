/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author chanatip
 */
public class OXProgram2UnitTest {

    public OXProgram2UnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinPlayer_O_output_false() {
        char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(false, OXProgram2.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRow1Player_O_output_true() {
        char[][] table = {{'O', 'O', 'O'}, {'-', '-', '-'}, {'-', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram2.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRow2Player_O_output_true() {
        char[][] table = {{'-', '-', '-'}, {'O', 'O', 'O'}, {'-', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram2.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRow3Player_O_output_true() {
        char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'O', 'O', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram2.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinPlayer_X_output_false() {
        char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(false, OXProgram2.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRow1Player_X_output_true() {
        char[][] table = {{'X', 'X', 'X'}, {'-', 'O', 'O'}, {'-', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram2.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRow2Player_X_output_true() {
        char[][] table = {{'-', 'O', 'O'}, {'X', 'X', 'X'}, {'-', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram2.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRow3Player_X_output_true() {
        char[][] table = {{'-', '-', '-'}, {'-', 'O', 'O'}, {'X', 'X', 'X'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram2.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinCol1Player_X_output_true() {
        char[][] table = {{'X', 'O', 'O'}, {'X', '-', '-'}, {'X', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram2.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinCol2Player_X_output_true() {
        char[][] table = {{'O', 'X', 'O'}, {'-', 'X', '-'}, {'-', 'X', '-'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram2.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinCol3Player_X_output_true() {
        char[][] table = {{'O', 'O', 'X'}, {'-', '-', 'X'}, {'-', '-', 'X'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram2.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinCol1Player_O_output_true() {
        char[][] table = {{'O', '-', '-'}, {'O', 'X', 'X'}, {'O', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram2.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinCol2Player_O_output_true() {
        char[][] table = {{'-', 'O', '-'}, {'X', 'O', 'X'}, {'-', 'O', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram2.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinCol3Player_O_output_true() {
        char[][] table = {{'-', '-', 'O'}, {'X', 'X', 'O'}, {'-', '-', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram2.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinSlantRightPlayer_X_output_true() {
        char[][] table = {{'-', '-', 'X'}, {'O', 'X', 'O'}, {'X', '-', 'O'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram2.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinSlantLeftPlayer_X_output_true() {
        char[][] table = {{'X', '-', 'O'}, {'-', 'X', 'O'}, {'-', '-', 'X'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram2.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinSlantRightPlayer_O_output_true() {
        char[][] table = {{'-', '-', 'O'}, {'X', 'O', '-'}, {'O', '-', 'X'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram2.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinSlantLeftPlayer_O_output_true() {
        char[][] table = {{'O', '-', '-'}, {'-', 'O', 'X'}, {'X', '-', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram2.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckDraw1_O_output_true() {
        char[][] table = {{'X', 'X', 'O'}, {'O', 'O', 'X'}, {'X', 'O', 'X'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram2.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckDraw2_O_output_true() {
        char[][] table = {{'X', 'O', 'X'}, {'X', 'X', 'O'}, {'O', 'X', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram2.checkWin(table, currentPlayer));
    }

}
