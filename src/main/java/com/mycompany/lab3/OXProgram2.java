/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab3;

/**
 *
 * @author chanatip
 */
class OXProgram2 {

    static boolean checkWin(char[][] table, char currentPlayer) {
        if (checkRow(table, currentPlayer) || checkCol(table, currentPlayer) || checkX1(table, currentPlayer) || checkX2(table, currentPlayer) || checkDraw(table, currentPlayer)) {
            return true;
        }
        return false;
    }

    private static boolean checkRow(char[][] table, char currentPlayer) {
        for (int row = 0; row < 3; row++) {
            if (checkRow(table, currentPlayer, row)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkRow(char[][] board, char currentPlayer, int row) {
        for (int i = 0; i < 3; i++) {
            if (board[row][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol(char[][] board, char currentPlayer) {
        for (int col = 0; col < 3; col++) {
            if (checkCol(board, currentPlayer, col)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkCol(char[][] board, char currentPlayer, int col) {
        for (int i = 0; i < 3; i++) {
            if (board[i][col] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX1(char[][] board, char currentPlayer) {
        for (int i = 0; i < 3; i++) {
            if (board[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2(char[][] board, char currentPlayer) {
        for (int i = 0; i < 3; i++) {
            if (board[i][3 - i - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDraw(char[][] board, char currentPlayer) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

}
